import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import CalculationFieldListD from '../CalculationFieldListD';
import {addCF} from '../../actionCreators/index';
import cyrillicToTranslit from 'cyrillic-to-translit-js';
import {Button} from 'react-bootstrap';

import './style.css';

class CounterFieldList extends Component {
    static propTypes = {
        //from redux
        calculationFields: PropTypes.array,
        selectedValues: PropTypes.array,
        addCF: PropTypes.func
    };
    render() {
        return (
            <div>
                <Button bsStyle="primary" onClick={this.handleClick} name={'counter'}>Add counter</Button>
                <Button onClick={this.handleClick} name={'formula'}>Add formula</Button>
                <div className={'cf-fields'}>

                    {this.getBody()}
                </div>
            </div>
        );
    }

    getBody = () => {
        return <CalculationFieldListD />;
    }

    handleClick = (e) => {
        const onlyRus = /([А-я]+)/g;
        const numbers = /\d/;
        const numToAlf = ['a','b','c','d','e','f','g','h','i','j'];
        const result = prompt('name', 'counter');
        if(result === null || result.length < 1) return;
        let numb = result.match(numbers);
        let rusWord = result.match(onlyRus);
        if(rusWord === null) {
            let hiddenName = result;
            if(numb !== null) {
                for(let i = 0; i < 10; i++){
                    hiddenName = hiddenName.replace(new RegExp(i+'', 'g'), numToAlf[i]);
                }
            }
            this.props.addCF(hiddenName, e.target.name, result);
        } else {
            let hiddenName = cyrillicToTranslit().transform(result, '_');
            if(numb !== null) {
                for(let i = 0; i < 10; i++){
                    hiddenName = hiddenName.replace(new RegExp(i+'', 'g'), numToAlf[i]);
                }
            }
            this.props.addCF(hiddenName, e.target.name, result);
        }
    }
}

export default connect((state) => (
    {calculationFields: state.calculationFields,
        selectedValues: state.selectedValues}
), {addCF})(CounterFieldList);