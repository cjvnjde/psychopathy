import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setType} from '../actionCreators';
import {TYPES} from '../constants';
import {FormGroup, ControlLabel, FormControl, Form} from 'react-bootstrap';
class TypeChooser extends Component {
    static propTypes = {
        //from redux
        setType: PropTypes.func,
        questionsType: PropTypes.string
    };

    render() {
        return (
            <div>
                <Form>
                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Choose type</ControlLabel>
                        <FormControl componentClass="select" onChange={this.handleChange} value={this.props.questionsType}>
                            {this.getBody()}
                        </FormControl>
                    </FormGroup>
                </Form>
            </div>
        );
    }

    getBody() {
        let res = [];
        for(let i = 0; i < TYPES.length; i++){
            res[i] = <option key={TYPES[i]} value={TYPES[i]}>{TYPES[i]}</option>;
        }
        return res;
    }

    handleChange = (e) => {
        this.props.setType(e.target.value);
    }


}

export default connect((state) => ({questionsType: state.questionsType}), {setType})(TypeChooser);