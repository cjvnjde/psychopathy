import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setValue} from '../../actionCreators/index';

import './style.css';

class Index extends Component {
    static propTypes = {
        selected: PropTypes.number,
        id: PropTypes.number,
        //from redux
        range: PropTypes.object,
        setValue: PropTypes.func
    };

    render() {
        if(this.props.range.min > this.props.selected){
            this.props.setValue(this.props.id, parseInt(this.props.range.min));
        }
        if(this.props.selected > this.props.range.max){
            this.props.setValue(this.props.id, parseInt(this.props.range.max));
        }
        return (
            <div>
                <select className={'select-number'}
                    value={this.props.selected}
                    onChange={this.handleChange} onKeyPress={this.handleKey}>
                    {this.getBody()}
                </select>
            </div>
        );
    }
    handleKey = (e) => {
        let num = Number(e.key);
        if(!isNaN(num) && num !== null && num >=0)
            this.props.setValue(this.props.id, num>this.props.range.max?this.props.range.max:num);
    }

    handleChange = (e) => {
        this.props.setValue(this.props.id, parseInt(e.target.value));
    };

    getBody() {
        let res = [];
        const {range} = this.props;
        for(let i = range.min; i <= range.max; i++){
            res[i] = <option key={i}>{i}</option>;
        }
        return res;
    }
}

export default connect((state) => ({range: state.range}), {setValue})(Index);