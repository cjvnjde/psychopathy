import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setRange} from '../actionCreators';
import {FormGroup, ControlLabel, FormControl, Form, Col} from 'react-bootstrap';

class numberRangeChooser extends Component {
    static propTypes = {
        //from redux
        range: PropTypes.object,
        setRange: PropTypes.func
    };

    render() {
        return (
            <div>
                <Form horizontal>
                    <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} xs={4} md={4}>
                            Minimum
                        </Col>
                        <Col xs={8} md={8}>
                            <FormControl type="number"  value={this.props.range.min} name={'min'} onChange={this.handleChange}/>
                        </Col>
                    </FormGroup>

                    <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} xs={4} md={4}>
                            Maximum
                        </Col>
                        <Col xs={8} md={8}>
                            <FormControl type="number" name={'max'} value={this.props.range.max} onChange={this.handleChange}/>
                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }

    handleChange = (e) => {
        switch(e.target.name) {
        case 'min': {
            if(e.target.value >=  this.props.range.max)
                this.props.setRange(parseInt(e.target.value), parseInt(e.target.value)+1);
            else
                this.props.setRange(parseInt(e.target.value), this.props.range.max);
        }
            break;
        case 'max': this.props.setRange(this.props.range.min, parseInt(e.target.value));
            break;
        }
    }


}

export default connect((state) => ({range: state.range}), {setRange})(numberRangeChooser);