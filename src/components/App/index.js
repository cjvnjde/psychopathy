import React, {Component} from 'react';
import Inquirer from '../Inquirer';
import Settings from '../Settings/index';
import {Navbar, Nav, NavItem, Row, Col, Grid} from 'react-bootstrap';
import Sticky from '../react-sticky-el/es';
import Histogram from '../Histogram';

import './style.css';
import './bootstrap.css';
import './bootstrap-theme.css';


class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            areSettingsOpen: true,
            areSettingsToggled: false,
            areDiagrams: false
        };
    }
    render() {
        return(
            <div>

                <Navbar collapseOnSelect>
                    <Navbar.Header>
                        <Navbar.Brand>
                            <a href="#">Psychopathy</a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight>
                            <NavItem eventKey={1} href="#" onClick={this.toggleDiagrams}>
                                Diagrams
                            </NavItem>
                            <NavItem eventKey={2} href="#" onClick={this.toggleSettings}>
                                Settings
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
                <Grid>
                    <Row className="show-grid">
                        {this.getMainPage()}
                    </Row>
                </Grid>
            </div>
        );
    }

    getMainPage() {
        if(!this.state.areDiagrams){
            return <div>
                {this.getSettingsBody()}
                {this.getContentBody()}
            </div>;
        }else{
            return <div>
                <Histogram width={800} height={500} data={[{name: 'A', value: 234}, {name: 'B', value: 134}, {name: 'C', value: 199}, ]}/>
                <Histogram width={150} height={150} data={[{name: 'A', value: 234}, {name: 'B', value: 134}, {name: 'C', value: 199}, ]}/>
            </div>;
            //return <DiagramsList />
        }
    }


    getSettingsBody() {
        if(!this.state.areSettingsOpen) return null;
        return (
            <Col  md={4} mdPush={8}>
                <Sticky>
                    <div className={'arrow-d noselect'} onClick={this.toggleSettings2}>
                        <span className={this.state.areSettingsToggled?'arrow-down':'arrow-up'} >&lt;</span>
                    </div>
                    {this.getSetBody()}
                </Sticky>

            </Col>);
    }

    getSetBody = () => {
        if(this.state.areSettingsToggled){
            return null;
        }else{
            return(
                <div className={'settings-container'}>
                    <Settings/>
                </div>
            );
        }
    }

    toggleSettings2 = () => {
        this.setState(
            {
                areSettingsToggled: !this.state.areSettingsToggled
            }
        );
    }

    getContentBody() {
        if(!this.state.areSettingsOpen) {
            return (
                <Col>
                    <div className={'content-container'}>
                        <div>
                            <Inquirer/>
                        </div>
                    </div>
                </Col>
            );
        } else {
            return (
                <Col  md={8} mdPull={4}>
                    <div className={'content-container'}>
                        <div>
                            <Inquirer/>
                        </div>
                    </div>
                </Col>
            );
        }
    }


    toggleDiagrams = () => {
        this.setState({
            areDiagrams: !this.state.areDiagrams
        });
    }

    toggleSettings = () => {
        this.setState({
            areSettingsOpen: !this.state.areSettingsOpen
        });
    }
}


export default App;