import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as d3 from 'd3';

import './style.css';

class Diagram extends Component {
    static propTypes = {
        height: PropTypes.number,
        width: PropTypes.number,
        data: PropTypes.array
    };

    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <div>
                {this.getBody()}
            </div>
        );
    }

    getBody = () => {
        const {data} = this.props;
        const margin = {top: 20, right: 30, bottom: 35, left: 40};
        const width = this.props.width - margin.left - margin.right;
        const height=this.props.height - margin.top - margin.bottom;
        const x = d3.scaleBand().rangeRound([0, width]).padding(0.1);
        const y = d3.scaleLinear().rangeRound([height, 0]);

        x.domain(data.map(d => d.name));
        y.domain([0, d3.max(data, d => d.value)]);

        const bars = data.map(
            d =>
                <g transform={`translate(${margin.left}, ${margin.top})`} key={d.name+d.value}>
                    <g className={'h-axis h-axis--x'} transform={`translate(0, ${height})`}
                        ref={node => d3.select(node).call(d3.axisBottom(x))}/>
                    <g className={'h-axis h-axis--y'}
                        ref={node => d3.select(node).call(d3.axisLeft(y))}>

                    </g>
                    <rect className={'h-bar'}
                        x={x(d.name)}
                        y={y(d.value)}
                        width={x.bandwidth()}
                        height={height - y(d.value)}/>
                    <text fill="#000" y={height+margin.top+margin.bottom/3} x={x(d.name)+x.bandwidth()/2}>{d.value}</text>
                </g>
        );

        return (<svg width={width+ margin.left + margin.right}
            height={height  + margin.top + margin.bottom}>
            {bars}
        </svg>);
    }

}

export default Diagram;