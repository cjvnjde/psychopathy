import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import CalculationField from './CalculationField/index';
import {connect} from 'react-redux';
import {addCF, setCF, reorderCF} from '../actionCreators';
import {Parser} from 'hot-formula-parser';
import PropTypes from "prop-types";
let parser = new Parser();

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    //padding: grid * 2,
    //margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    background: isDragging ? 'grey' : 'blue',

    // styles we need to apply on draggables
    ...draggableStyle,
});

class CalculationFieldListD extends Component {
    static propTypes = {
        //from redux
        calculationFields: PropTypes.array,
        selectedValues: PropTypes.array,
        reorderCF: PropTypes.func
    };
    constructor(props) {
        super(props);

    }

    render() {
        const CF = this.getBody();
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                        <div
                            ref={provided.innerRef}
                        >

                            {CF.map((item, index) => (
                                <Draggable key={item.name} draggableId={item.name} index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getItemStyle(
                                                snapshot.isDragging,
                                                provided.draggableProps.style
                                            )}
                                        >
                                            {item.content}
                                        </div>
                                    )}
                                </Draggable>
                            ))}

                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
        );
    }

    getBody = () => {
        const CF = this.props.calculationFields;
        let res = [];
        for(let i = 0; i < CF.length; i++){

            let val = 0;
            if(CF[i].type === 'counter'){
                val = CF[i].input.split(',').reduce((sum, current) => {
                    let num = Number(current);
                    if(!isNaN(num) && num > 0){
                        return sum + this.props.selectedValues[num-1];
                    }
                    return sum;
                }, 0);
            }else if(CF[i].type === 'formula'){
                let input = CF[i].input;
                for(let j = 0; j < CF.length; j++){
                    input = input.replace(CF[j].visibleName, CF[j].name);
                }
                let res = parser.parse(input).result;
                if(res === '' || res === null) res = 0;
                val = res;
            }
            parser.setVariable(CF[i].name, val);

            res.push({
                content: <CalculationField  name={CF[i].name}
                    visibleName={CF[i].visibleName}
                    value={val}
                    key={CF[i].name}
                    input={CF[i].input}
                    className={CF[i].type==='counter'?'counter':'formula'}
                />,
                name: CF[i].name}
            );
        }
        return res;
    }



    onDragEnd = (result) => {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        this.props.reorderCF(
            result.source.index,
            result.destination.index
        );



    }

}

export default connect((state) => (
    {calculationFields: state.calculationFields,
        selectedValues: state.selectedValues}
), {addCF, setCF, reorderCF})(CalculationFieldListD);

