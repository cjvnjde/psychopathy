import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setCF, deleteCF} from '../../actionCreators/index';

import './style.css';

class CF extends Component {
    static propTypes = {
        name: PropTypes.string,
        value: PropTypes.number,
        input: PropTypes.string,
        className: PropTypes.string,
        visibleName: PropTypes.string,
        //from redux
        setCF: PropTypes.func,
        deleteCF: PropTypes.func
    };

    render() {
        return (
            <div className={this.props.className}>
                <a className={'del-btn'} href={'#'} onClick={this.handleClick}><span /></a>
                {this.props.visibleName} = <input name={this.props.name}
                    type={'string'}
                    value={this.props.input}
                    onChange={this.handleChange}/>
                <span className={'result'}>{this.props.value}</span>
            </div>
        );
    }
    handleClick = () => {
        this.props.deleteCF(this.props.name);
    }
    handleChange = (e) => {
        let input  = e.target.value;
        this.props.setCF(this.props.name, input);
    }
}

export default connect(null, {setCF, deleteCF})(CF);