import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {hideQuestions} from '../actionCreators';

class QuestionsHidden extends Component {
    static propTypes = {
        //from redux
        isHidden: PropTypes.bool,
        hideQuestions: PropTypes.func
    };
    render() {
        return (
            <div>
                <input type={'checkbox'} onChange={this.props.hideQuestions} checked={this.props.isHidden}></input> Show/Hide questions text
                {/*<input type={'checkbox'} checked={this.props.isHidden} onChange={this.props.hideQuestions()}>Hide questions</input>*/}
            </div>
        );
    }


}

export default connect((state) => ({
    isVisible: state.isVisible
}),{hideQuestions})(QuestionsHidden);
