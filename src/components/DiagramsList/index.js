import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Diagram from '../Diagram/index';
import {Parser} from 'hot-formula-parser';

import './style.css';


let parser = new Parser();
class Index extends Component {
    static propTypes = {
        calculationFields: PropTypes.any,
        selectedValues: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.state = {
            diagrams: [],
            variables: []
        };
    }
    UNSAFE_componentWillMount() {
        this.state.getVariables();
    }
    render() {
        return (
            <div>
                <a className={'add-diagram'} href={'#'} onClick={this.handleClick}><span /></a>
                {this.getBody()}
            </div>
        );
    }
    getBody = () => {
        let ret = [];
        for(let i = 0; i < this.state.diagrams.length; i++){
            ret.push(
                <Diagram height={this.state.diagrams[i].h} width={this.state.diagrams[i].w} key={this.state.diagrams[i].h*this.state.diagrams[i].w}/>
            );
        }
        return ret;
    }
    handleClick = () => {
        let result = prompt('width, height', '800, 500');
        result = result.split(',');
        this.setState({
            diagrams: [...this.state.diagrams, {w: +result[0], h: +result[1]}]
        });
    }

    getVariables = () => {
        const CF = this.props.calculationFields;
        let res = [];
        for(let i = 0; i < CF.length; i++){

            let val = 0;
            if(CF[i].type === 'counter'){
                val = CF[i].input.split(',').reduce((sum, current) => {
                    let num = Number(current);
                    if(!isNaN(num) && num > 0){
                        return sum + this.props.selectedValues[num-1];
                    }
                    return sum;
                }, 0);
            }else if(CF[i].type === 'formula'){
                let input = CF[i].input;
                for(let j = 0; j < CF.length; j++){
                    input = input.replace(CF[j].visibleName, CF[j].name);
                }
                let res = parser.parse(input).result;
                if(res === '' || res === null) res = 0;
                val = res;
            }
            parser.setVariable(CF[i].name, val);

            res.push({
                name: CF[i].visibleName,
                val: val
            });
        }
        this.setState({
            variables: res
        });
    }

}

export default connect((state) => (
    {calculationFields: state.calculationFields,
        selectedValues: state.selectedValues}
), null)(Index);