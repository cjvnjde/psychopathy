import React, {Component} from 'react';
import Question from './Question/index';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

class Inquirer extends Component {
    static propTypes = {
        //from redux
        questions: PropTypes.array,
        selected: PropTypes.array,
        isVisible: PropTypes.bool
    };
    render() {
        //console.log(this.props.selected);
        return (
            <div>
                {this.getBody()}
            </div>
        );
    }

    getBody = () => {
        let ret = [];
        for(let i = 0; i < this.props.questions.length; i++){
            ret[i] = <Question text={!this.props.isVisible?this.props.questions[i]:''}
                key={this.props.questions[i]}
                selected={this.props.selected[i]} id={i}/>;
        }

        return ret;
    }
}

export default connect((state) => ({
    questions: state.questions,
    selected: state.selectedValues,
    isVisible: state.isVisible
}),null)(Inquirer);
