import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {setValue} from '../../actionCreators/index';

import './style.css';

class Index extends Component {
    static propTypes = {
        selected: PropTypes.number,
        id: PropTypes.number,
        //from redux
        setValue: PropTypes.func
    };


    render() {
        if(this.props.selected > 1){
            this.props.setValue(this.props.id, 1);
        }

        return (
            <div className={'yes-no'}>
                <input type="radio" value="yes"
                    checked={this.props.selected === 1}
                    onChange={this.handleOptionChange}/>
                <label>Yes</label>
                <input type="radio" value="no"
                    checked={this.props.selected === 0}
                    onChange={this.handleOptionChange}/>
                <label>No</label>
            </div>);
    }

    handleOptionChange = (e) => {
        let val = 0;
        switch (e.target.value) {
        case 'yes': val = 1;
            break;
        case 'no': val = 0;
            break;
        }
        this.props.setValue(this.props.id, val);
    }
}

export default connect(null, {setValue})(Index);