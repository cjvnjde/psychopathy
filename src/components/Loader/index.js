import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {addQuestions, setValue} from '../../actionCreators/index';
import {connect} from 'react-redux';

import './style.css';

class Index extends Component {
    static propTypes = {
        //from redux
        addQuestions: PropTypes.func,
        setValue: PropTypes.func
    };

    render() {
        return (
            <div>
                <label className={'btn btn-default btn-file'}>
                    Browse... <input className={'no-display'} type={'file'} onChange={this.handleChange}/>
                </label>

            </div>
        );
    }

    handleChange = (e) => {
        const file = e.target.files;
        const reader = new FileReader();

        reader.onload = () => {
            const text = reader.result;
            const parsedText = this.parseData(text);
            //load to redux
            this.props.addQuestions(parsedText);
            parsedText.forEach((item, i) => {
                this.props.setValue(i, 0);
            });
        };

        if(file.length === 1)
            reader.readAsText(file[0]);
    };

    parseData = (text) => {
        let arr = text.split(/\r?\n/);

        arr = arr.filter(element => {
            return element.length > 3;
        });

        arr = arr.map((element) => {
            return element.trim();
        });

        return arr;
    };

}

export default connect(null, {addQuestions, setValue})(Index);