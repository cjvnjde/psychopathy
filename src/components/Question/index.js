import React, {Component} from 'react';
import PropTypes from 'prop-types';
import QuestionTypeNum from '../QuestionTypeNum/index';
import QuestionTypeBool from '../QuestionTypeBool/index';
import {connect} from 'react-redux';

import './style.css';

class Index extends Component {
    static propTypes = {
        text: PropTypes.string,
        selected: PropTypes.number,
        id: PropTypes.number,
        //from redux
        type: PropTypes.string
    };

    render() {
        return (
            <div className={'question-block'}>
                {this.props.text}
                <div className={'block-1'}>
                    {this.props.id+1}
                    {this.getTypeBody()}
                </div>
            </div>
        );
    }

    getTypeBody() {
        let questionType = null;
        switch(this.props.type) {
        case 'num': questionType = <QuestionTypeNum id={this.props.id} selected={this.props.selected}/>;
            break;
        case 'bool': questionType = <QuestionTypeBool id={this.props.id} selected={this.props.selected}/>;
            break;
        }
        return questionType;
    }
}

export default connect((state) => ({
    type: state.questionsType
}))(Index);