import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Parser} from 'hot-formula-parser';
import * as d3 from 'd3';

import './style.css';

let parser = new Parser();


class Index extends Component {
    static propTypes = {
        height: PropTypes.number,
        width: PropTypes.number,
        calculationFields: PropTypes.any,
        selectedValues: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.state = {
            variables: [],
            usedVariables: []
        };
    }
    UNSAFE_componentWillMount() {
        this.getVariables();
    }

    render() {
        return (
            <div>
                <a className={'add-variables'} href={'#'} onClick={this.handleClick}><span /></a>
                {this.state.variables.map(d => d.name+'='+d.val+'; ')}
                <br/>
                {this.getBody()}
            </div>
        );
    }

    getBody = () => {

        let data = [];
        for(let i = 0; i < this.state.usedVariables.length; i++){
            for(let j = 0; j < this.state.variables.length; j++){
                if(this.state.usedVariables[i] === this.state.variables[j].name){
                    data.push(this.state.variables[j]);
                }
            }
        }

        const margin = {top: 20, right: 30, bottom: 30, left: 40};
        const width = this.props.width - margin.left - margin.right;
        const height=this.props.height - margin.top - margin.bottom;
        const x = d3.scaleBand().rangeRound([0, width]).padding(0.1);
        const y = d3.scaleLinear().rangeRound([height, 0]);

        x.domain(data.map(d => d.name));
        y.domain([0, d3.max(data, d => d.val)]);

        data = data.map(
            d =>
                <g transform={`translate(${margin.left}, ${margin.top})`} key={d.name}>
                    <g className={'axis axis--x'} transform={`translate(0, ${height})`}
                        ref={node => d3.select(node).call(d3.axisBottom(x))}/>
                    <g className={'axis axis--y'}
                        ref={node => d3.select(node).call(d3.axisLeft(y))}>
                        <text transform={'rotate(-90)'}
                            y={'6'}
                            dy={'0.71em'}
                            textAnchor={'end'}>
                        </text>
                    </g>
                    <rect className={'bar'}
                        x={x(d.name)}
                        y={y(d.val)}
                        width={x.bandwidth()}
                        height={height - y(d.val)}/>
                </g>
        );

        return (<svg width={width+ margin.left + margin.right}
            height={height  + margin.top + margin.bottom}>
            {data}
        </svg>);
    }
    handleClick = () => {
        let res = prompt('add one var', 'A');
        this.setState({
            usedVariables: [...this.state.usedVariables, res]
        });
    }

    getVariables = () => {
        const CF = this.props.calculationFields;
        let res = [];
        for(let i = 0; i < CF.length; i++){

            let val = 0;
            if(CF[i].type === 'counter'){
                val = CF[i].input.split(',').reduce((sum, current) => {
                    let num = Number(current);
                    if(!isNaN(num) && num > 0){
                        return sum + this.props.selectedValues[num-1];
                    }
                    return sum;
                }, 0);
            }else if(CF[i].type === 'formula'){
                let input = CF[i].input;
                for(let j = 0; j < CF.length; j++){
                    input = input.replace(CF[j].visibleName, CF[j].name);
                }
                let res = parser.parse(input).result;
                if(res === '' || res === null) res = 0;
                val = res;
            }
            parser.setVariable(CF[i].name, val);

            res.push({
                name: CF[i].visibleName,
                val: val
            });
        }
        this.setState({
            variables: res
        });
    }
}

export default connect((state) => (
    {calculationFields: state.calculationFields,
        selectedValues: state.selectedValues}
), null)(Index);