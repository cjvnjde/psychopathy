import React, {Component} from 'react';
import Loader from '../Loader/index';
import TypeChooser from '../TypeChooser';
import NumberRangeChooser from '../NumberRangeChooser';
import QuestionsHidden from '../QuestionsHidden';
import CalculationFieldList from '../CalculationFieldList/index';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import './style.css';

class Index extends Component {
    static propTypes = {
        questionsType: PropTypes.string
    };

    render() {

        return (
            <div className={'before-styk'}>
                {this.getSettingsBody()}
            </div>
        );
    }

    getSettingsBody = () => {
        return (
            <div>
                <Loader/>
                <QuestionsHidden/>
                <TypeChooser/>
                {this.getNumRange()}
                <CalculationFieldList/>
            </div>
        );

    }


    getNumRange = () => {
        let numRange = <NumberRangeChooser />;
        if(this.props.questionsType === 'num'){
            return numRange;
        }
        return null;
    }
}

export default connect((state) => ({questionsType: state.questionsType}))(Index);