import {combineReducers} from 'redux';
import questions from './questions';
import questionsType from './questionsType';
import selectedValues from './selectedValues';
import range from './range';
import isVisible from './isVisible';
import calculationFields from './calculationFields';

export default combineReducers({
    questions,
    questionsType,
    selectedValues,
    range,
    calculationFields,
    isVisible
});