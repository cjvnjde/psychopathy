import {HIDE_QUESTIONS} from '../constants';

export default (isHidden = false, action) => {
    const {type, payload} = action;

    switch (type) {
    case HIDE_QUESTIONS: {
        return !isHidden;
    }
    }

    return isHidden;
};