import {SET_TYPE, TYPES} from '../constants';

export default (questionsType = TYPES[0], action) => {
    const {type, payload} = action;

    switch (type) {
    case SET_TYPE: {
        return payload.questionsType;
    }
    }

    return questionsType;
};