import {SET_VALUE} from '../constants';

export default (values = [0], action) => {
    const {type, payload} = action;

    switch (type) {
    case SET_VALUE: {
        const val = values.slice();
        val[payload.questionId] = payload.value;
        return val;
    }
    }

    return values;
};