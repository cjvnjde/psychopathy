import {SET_RANGE} from '../constants';

export default (range = {min: 0, max: 1}, action) => {
    const {type, payload} = action;

    switch (type) {
    case SET_RANGE: {
        return  {min: payload.min, max: payload.max};
    }
    }

    return range;
};