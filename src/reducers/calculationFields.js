import {ADD_CF, SET_CF, DELETE_CF, MOVE_CF, REORDER_CF} from '../constants';

export default (cf = [], action) => {
    const {type, payload} = action;

    switch (type) {
    case ADD_CF: {
        let _cf = cf.slice();
        for(let i = 0; i < _cf.length; i++){
            if(_cf[i].name === payload.name) return _cf;
        }
        _cf.push({
            name: payload.name,
            visibleName: payload.visibleName===undefined?payload.name:payload.visibleName,
            input: '',
            type: payload.type
        });
        return _cf;
    }
    case SET_CF: {
        let _cf = cf.slice();
        for(let i = 0; i < _cf.length; i++){
            if(_cf[i].name === payload.name){
                _cf[i].input = payload.input;
            }
        }
        return _cf;
    }
    case DELETE_CF: {
        let _cf = cf.slice();
        _cf = _cf.filter((element) => {
            return element.name !== payload.name;
        });
        return _cf;
    }
    case REORDER_CF: {
        // const reorder = (list, startIndex, endIndex) => {
        const result = Array.from(cf);
        const [removed] = result.splice(payload.startIndex, 1);
        result.splice(payload.endIndex, 0, removed);
        return result;
        // };

    }
    case MOVE_CF: {
        let _cf = cf.slice();
        for(let i = 0; i < _cf.length; i++){
            if(_cf[i].name === payload.name) {
                if(payload.direction === 'up' && i > 0){
                    let temp = _cf[i-1];
                    _cf[i-1] = _cf[i];
                    _cf[i] = temp;
                    return _cf;
                } else if(payload.direction === 'down' && i+1 < _cf.length) {
                    let temp = _cf[i+1];
                    _cf[i+1] = _cf[i];
                    _cf[i] = temp;
                    return _cf;
                }

            }
        }
        return _cf;
    }
    }

    return cf;
};
