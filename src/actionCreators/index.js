import {ADD_QUESTIONS, SET_TYPE, SET_VALUE,
    SET_RANGE, ADD_CF, SET_CF, HIDE_QUESTIONS,
    DELETE_CF, MOVE_CF, REORDER_CF} from '../constants';

export function addQuestions(questions) {
    return {
        type: ADD_QUESTIONS,
        payload: {questions}
    };
}

export function setType(questionsType) {
    return {
        type: SET_TYPE,
        payload: {questionsType}
    };
}

export function setValue(questionId, value) {
    return {
        type: SET_VALUE,
        payload: {questionId, value}
    };
}

export function setRange(min, max) {
    return {
        type: SET_RANGE,
        payload: {min, max}
    };
}

export function addCF(name, type, visibleName) {
    return {
        type: ADD_CF,
        payload: {name, type, visibleName}
    };
}

export function setCF(name, input) {
    return {
        type: SET_CF,
        payload: {name, input}
    };
}

export function deleteCF(name) {
    return {
        type: DELETE_CF,
        payload: {name}
    };
}

export function moveCF(name, direction) {
    return {
        type: MOVE_CF,
        payload: {name, direction}
    };
}

export function reorderCF(startIndex, endIndex) {
    return {
        type: REORDER_CF,
        payload: {startIndex, endIndex}
    };
}

export function hideQuestions() {
    return {
        type: HIDE_QUESTIONS,
    };
}